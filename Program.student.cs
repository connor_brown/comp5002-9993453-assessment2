using System;

namespace comp5002_9993453_assessment2
{
    partial class Program
    {
        public class student : person
        {
           public string subject;
           public student(string _name, string _username, int _ID, string _subject)
           : base(_name, _username, _ID)
            {
                subject = _subject;
            }
           public void listSubjects()
           {
               Console.WriteLine($"{subject}");
           }
        }
    }
}
