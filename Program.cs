﻿using System.Collections.Generic;

namespace comp5002_9993453_assessment2
{
    partial class Program
    {

        static void Main(string[] args)
        {
            var subjects = new List<subject>
            {
                new subject
                {Subject ="Math", CourseCode = "Mat", Teacher = "Forbes"},

                new subject
                {Subject ="English", CourseCode = "Eng", Teacher = "Ibon"},

                new subject
                {Subject ="Biology", CourseCode = "Bio", Teacher = "Jackson"},

                new subject
                {Subject ="Drama", CourseCode = "Dra", Teacher = "Kain"},
            };
            var teachers = new List<teacher>{
                new teacher("Forbes", "Dak5", 9001, "Math"),
                new teacher("Ibon", "Misspiggy", 9002, "English"),
                new teacher("Jackson", "Kingkong", 9003, "Biology"),
                new teacher("Kain", "F@mous", 9004, "Drama"),
                };

            var students = new List<student>(){
            new student("Alex", "king", 1001, "English"),
            new student("Kaos", "Hillbillybob", 1002, "Math"),
            new student("Ant", "Tzeeshful", 1003, "Biology"),
            new student("lucian", "Warwick", 1004, "Biology"),
            new student("Max", "Hentai", 1005, "Math"),
            new student("Lily", "Conan", 1006, "Drama"),
            new student("Amy", "Demon", 1007, "English"),
            new student("Mia", "linix", 1008, "Drama"),
            };

            foreach(var teacher in teachers) 
            {
                System.Console.WriteLine(teacher.name + " : " + teacher.subject);
            }
                System.Console.WriteLine(" ");
            foreach(var student in students) 
            {
                System.Console.WriteLine(student.name + " : " + student.subject);
            }
        }

    }
}
