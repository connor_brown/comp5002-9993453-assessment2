using System;

namespace comp5002_9993453_assessment2
{
    partial class Program
    {
        public class teacher : person
        {
           public string subject;
           public teacher(string _name, string _username, int _ID, string _subject)
           : base(_name, _username, _ID) 
            {
                subject = _subject;
            }

            public teacher () : base("string","string",1){}
            

           void listSubjectsITeach()
           {
               Console.WriteLine($"{subject}");
           }
        }
    }
}
